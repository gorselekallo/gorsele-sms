"use strict";
var http = require("http");
var querystring = require("querystring");

var gorseleSMS = function() {
  this.config = {
    messageBirdAPIKey: null,
    gorseleUser: null,
    gorselePassword: null,
    useGorseleSMSAPI: null
  };

  this.useConfig = function(config) {
    for (var prop in config) {
      if (this.config[prop] !== "undefined") {
        this.config[prop] = config[prop];
      }
    }
  };

  this.send = function(origin, recipients, body, forceConfig, next) {
    if (recipients && recipients.length) {
      var countryBE = recipients[0].substr(0, 2) == "32";
      countryBE = true;
      var theConfig = forceConfig
        ? forceConfig
        : JSON.parse(JSON.stringify(this.config));
      if (countryBE && theConfig.useGorseleSMSAPI) {
        var recipient = "00" + recipients[0];
        if (recipient.indexOf("475497288") > -1) {
          return next(null, {
            gorsele: true,
            success: 1,
            text: "No sending to this number"
          });
        } else {
          if (recipient != "0032475497288") {
            var messageParams = {
              logon: theConfig.gorseleUser,
              pass: theConfig.gorselePassword,
              gsmnr: recipient,
              msgcontent: ""
            };
            var requestURL = "http://smsapi.gorsele.com:22281/smsin/smsin.asp";
            requestURL +=
              "?" + querystring.stringify(messageParams) + escape(body);
            performRequest(requestURL, null, function(err, gResponse) {
              if (err) {
                err.gorsele = true;
                return next(err, null);
              }
              gResponse.gorsele = true;
              return next(null, gResponse);
            });
          } else {
            return next(null, {
              gorsele: true,
              success: 1,
              text: "No sending to this number"
            });
          }
        }
      } else {
        var recipient = recipients[0];
        if (recipient.indexOf("475497288") > -1) {
          return next(null, {
            messagebird: true,
            success: 1,
            text: "No sending to this number"
          });
        } else {
          if (recipient != "32475497288") {
            var mb = require("messagebird")(theConfig.messageBirdAPIKey);
            var params = {
              originator: origin && origin != "" ? origin : "32495271096",
              recipients: recipients,
              body: body,
              datacoding: "auto"
            };
            mb.messages.create(params, function(err, mbResponse) {
              if (err) {
                err.success = 0;
                err.messagebird = true;
                return next(err, null);
              }
              mbResponse.success = 1;
              mbResponse.messagebird = true;
              return next(null, mbResponse);
            });
          } else {
            return next(null, {
              messagebird: true,
              success: 1,
              text: "No sending to this number"
            });
          }
        }
      }
    } else {
      return next(null, { result: "No number to send to" });
    }
  };
};

function performRequest(requestURL, params, next) {
  var endpoint = requestURL;
  if (params) {
    endpoint += "?" + querystring.stringify(params);
  }
  var req = http.get(endpoint, function(res) {
    res.setEncoding("utf-8");
    var responseString = "";

    res.on("data", function(data) {
      responseString += data;
    });

    res.on("end", function() {
      if (responseString) {
        var parseString = require("xml2js").parseString;
        parseString(responseString, function(err, responseObject) {
          if (err) {
            return next(err, null);
          }
          if (
            responseObject &&
            responseObject.smscomfort &&
            responseObject.smscomfort.status &&
            responseObject.smscomfort.status.length &&
            responseObject.smscomfort.status[0] == "accepted"
          ) {
            return next(null, { success: 1, response: responseObject });
          } else {
            if (
              responseObject &&
              responseObject.smscomfort &&
              responseObject.smscomfort.status &&
              responseObject.smscomfort.status.length &&
              responseObject.smscomfort.status[0] != "accepted"
            ) {
              return next(null, { success: 0, response: responseObject }, null);
            } else {
              return next(
                { error: "Unable to send Gorselé sms", result: responseObject },
                null
              );
            }
          }
        });
      }
    });
  });

  req.end();
  req.on("error", function(e) {
    console.error(e);
  });
}

/**
 * Expose `gorseleSMS()`.
 */
exports = module.exports = new gorseleSMS();
