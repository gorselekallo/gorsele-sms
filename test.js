var sms = require('./lib/');
require('dotenv').config({path: __dirname + '/config.env'});

sms.useConfig({
  messageBirdAPIKey   : process.env.MESSAGEBIRD_API_KEY,
  gorseleUser         : process.env.GORSELE_SMS_API_USER,
  gorselePassword     : process.env.GORSELE_SMS_API_PASSWORD,
  useGorseleSMSAPI    : process.env.GORSELE_SMS_API_ENABLED
});

var testMsg = 'Gorselé transport on 28/11: 13:00 Gasthuisstraat 1, Kallo -> Belgiëplein 1, Laken. Info: +3235709040';

sms.send(null, ['32488144722'], testMsg, function (err, result) {
  if(err){
    return console.log(err);
  }
  return console.log(result);
});